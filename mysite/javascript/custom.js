(function($) {

    // Document ready
    $(function() {
    	console.log('Sitename custom JS is ready !');

        // Close cookies box (very top header) on click "ok".
        $('.cookies_close').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            jQuery('#very-top-bar').slideUp();
            createCookie('showVeryTopHeader', 0, 0);
        });

        //[LDC-2021-08-13] #3454 on profil form, automaticly show other interest field if has been completed
        $FieldAddOtherInterestProfilForm = $('#IOPCustomRegisterForm_ProfileForm_IOPOtherInterest_Holder');
        if ($('input[name=IOPOtherInterest]').val()!==""){
            $('input[name=CheckBoxICantFindInterest]').attr('checked','checked');
            $FieldAddOtherInterestProfilForm.show();
        }

    });

    // [LDC-2021-06-21] #3443 show or hide field for other interests
    document.addEventListener('click',function(e){
        $FieldAddOtherInterest = $('#IOPCustomRegisterForm_RegisterForm_IOPOtherInterest_Holder');

        if( $('input[name=CheckBoxICantFindInterest]').is(':checked') ){
            $FieldAddOtherInterest.show();
        } else {
            $FieldAddOtherInterest.hide();
        }
    });

    document.addEventListener('click',function(e){
        $FieldAddOtherInterestProfilForm = $('#IOPCustomRegisterForm_ProfileForm_IOPOtherInterest_Holder');

        if( $('input[name=CheckBoxICantFindInterest]').is(':checked') ){
            $FieldAddOtherInterestProfilForm.show();
        } else {
            $FieldAddOtherInterestProfilForm.hide();
        }
    });

}(jQuery));