<?php
/**
 * 
 */
class RESTfulAPI_RCTokenAuthenticator extends RESTfulAPI_TokenAuthenticator
{

  const AUTH_CODE_ORIGIN_INVALID = 4;

  /**
   * Checks if a request to the API is authenticated
   * Gets API Token from HTTP Request and return Auth result
   * 
   * @param  SS_HTTPRequest           $request    HTTP API request
   * @return true|RESTfulAPI_Error                True if token is valid OR RESTfulAPI_Error with details
   */
  public function authenticate(SS_HTTPRequest $request)
  {
    // check cors domains
    $cors = Config::inst()->get('RESTfulAPI', 'cors');
    SS_Log::Log("RESTfulAPI_RCTokenAuthenticator.authenticate(". $request->getHeader('X-Silverstripe-Apitoken') ." / ". $this->get_client_ip() .", ". $cors['Allow-Origin'] .")", SS_Log::INFO);

    // skip if CORS is not enabled or all origins are allowed.
    if ( !$cors['Enabled'] || $cors['Allow-Origin'] === '*' ) {
      return parent::authenticate($request);
    }

    //check if origin is allowed
    $allowedOrigin = explode(',', $cors['Allow-Origin']);
    //if ( !is_array($cors['Allow-Origin']) ) $allowedOrigin = array($cors['Allow-Origin']);
    if ( in_array($this->get_client_ip(), $allowedOrigin) ) {
      return parent::authenticate($request);
    } else {
      return new RESTfulAPI_Error(403,
        'Origin invalid.',
        array(
          'message' => 'Origin invalid.',
          'code'    => self::AUTH_CODE_ORIGIN_INVALID
        )
      );
    }
  }

  public function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
      $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
      $ipaddress = 'UNKNOWN';
    return $ipaddress;
  }

}
