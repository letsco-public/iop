<?php

class RESTfulAPI_RCPermissionManager extends RESTfulAPI_DefaultPermissionManager
{
  /**
   * Checks if a given DataObject or Class
   * can be accessed with a given API request by a Member
   * 
   * @param  string|DataObject       $model       Model's classname or DataObject to check permission for
   * @param  DataObject|null         $member      Member to check permission agains
   * @param  string                  $httpMethod  API request HTTP method
   * @return Boolean                              true or false if permission was given or not
   */
  public function checkPermission($model, $member = null, $httpMethod)
  {
    // [JD-2019.06.20] Member must be 'Accepted' at least.
    if ($member && !$member->IsAccepted()) {
      SS_Log::Log("RESTfulAPI_RCPermissionManager.checkPermission($model, $httpMethod) >> Member($member->ID) in not Accepted", SS_Log::INFO);
      return false;
    } 

    // Member must be on the same Subsite as model data. 
    if (strtoupper($httpMethod) != 'POST' && is_object($model) && $member) {

      //[LDC.2021.08.25] #3500
      if ($member->hasMethod('Subsites')) {

        $memberSubsites = array_keys($member->Subsites()->map()->toArray());
        SS_Log::Log("RESTfulAPI_RCPermissionManager.checkPermission($member->ID) >> object: ". get_class($model) .'('. $model->ID .')', SS_Log::INFO);
        if ($model->hasMethod('Subsite') && $model->SubsiteID != 0) {
          SS_Log::Log("RESTfulAPI_RCPermissionManager.checkPermission($model, $httpMethod) >> does $model->SubsiteID in subsite list of member (". json_encode($memberSubsites) .") ? ". (in_array($model->SubsiteID, $memberSubsites) ? "oui" : "non"), SS_Log::INFO);
          return in_array($model->SubsiteID, $memberSubsites);
        }
        if ($model->hasMethod('Subsites')) {
          foreach ($model->Subsites() as $subsite) {
            SS_Log::Log("RESTfulAPI_RCPermissionManager.checkPermission($model, $httpMethod) >> does one of the model ($subsite->ID) in subsite list of member (". json_encode($memberSubsites) .") ? ". (in_array($subsite->ID, $memberSubsites) ? "oui" : "non"), SS_Log::INFO);
            if (in_array($subsite->ID, $memberSubsites)) return true;
          }
          return false;
        }
      }
    }

    return parent::checkPermission($model, $member, $httpMethod);
  }
}