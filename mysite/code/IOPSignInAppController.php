<?php

/**
 * IOPSignInAppController
 *
 * @author Johann
 * @extends SignInAppController
 */
class IOPSignInAppController extends Extension
{
    public function update_callback($member, $App, $result)
    {
        mlbclog::info([__CLASS__, $member->Email, $App, $result]);
        // $result['nodes'] = [
        //     '47709' => [
        //         "type"=> "organisation",
        //         "title"=> "IEP Grenoble",
        //         "created"=> "1395743964",
        //         "path"=> "organisations\/iep-grenoble"
        //     ],
        //     '47710' => [
        //         "type"=> "contact",
        //         "title"=> " DEMOUSTIER, Dani\u00e8le",
        //         "created"=> "1395744027",
        //         "path"=> "contacts\/demoustier-daniele"
        //     ],
        // ];
        if (isset($result['nodes'])) {
            foreach ($result['nodes'] as $node) {
                switch ($node['type']) {
                    case 'organisation':
                        $member->SocialReason = $node['title'];
                        break;
                    case 'contact':
                        $title = explode(', ', $node['title']);
                        $member->Surname = $title[0];
                        $member->FirstName = $title[1];
                        break;
                    default:
                        break;
                }
            }
            $member->write();
        }
    }
}
