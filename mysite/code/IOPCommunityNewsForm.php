<?php

/**
 * IOPCommunityNewsForm
 *
 * @author Johann
 * @extends CommunityNewsForm
 */
class IOPCommunityNewsForm extends CommunityNewsForm
{
    public function __construct($controller, $name = 'NewsForm')
    {
        parent::__construct($controller, $name);

        $options = [];
        foreach (IOPCommunityNews::listIOPTypes() as $name => $params) {
            $options[$name] = '
                <div class="SexyChoiceForm-button '. $params['color'] .'" title="'. $params['description'] .'">
                    <span class="SexyChoiceForm-buttonIcon"></span>
                    <span class="SexyChoiceForm-buttonLabel">'. $params['title'] .'</span>
                </div>';
        }

        $IOPType = new HTMLOptionsetField('IOPType', _t("$name.IOPType", ""), $options);
        $IOPType->addExtraClass('SexyChoiceForm');
        $this->Fields()->unshift($IOPType);
        $this->getValidator()->setConstraint('IOPType', Constraint_required::create());
        $this->getValidator()->setConstraint('Interests', Constraint_required::create());
        //[LDC-2021.06.25] #3470 Do not set FeaturedImage as mandatory field until we fix the #3470 bug  : if there is a Gallery Folder the constraint is never validated
        //$this->getValidator()->setConstraint('FeaturedImage', Constraint_required::create());

        Requirements::customCss('
            ul.SexyChoiceForm { display: flex; flex-direction: row; flex-wrap: wrap; }
            ul.SexyChoiceForm li { width: calc(100% / 4); }
            ul.SexyChoiceForm .SexyChoiceForm-button { min-height: 50px;width: calc(100% - 40px); margin-left: 10px !important; }
            ul.SexyChoiceForm input:checked + label .SexyChoiceForm-button.purple { background: rebeccapurple; }
            ul.SexyChoiceForm input:checked + label .SexyChoiceForm-button.red { background: red; }
            ul.SexyChoiceForm input:checked + label .SexyChoiceForm-button.blue { background: lightblue; }
            ul.SexyChoiceForm input:checked + label .SexyChoiceForm-button.green { background: green; }
            ul.SexyChoiceForm .SexyChoiceForm-button {color: white; background-color: var(--primary-color);}
            ul.SexyChoiceForm .SexyChoiceForm-button:hover {background-color: var(--hover-color);}
        '); //#3422 change button color from grey to primary color

        // Requirements::customCss('
        //     ul.SexyChoiceForm { overflow: hidden; margin-bottom: 10px; }
        //     ul.SexyChoiceForm li { margin-bottom: 0; }
        //     ul.SexyChoiceForm .SexyChoiceForm-button { min-height: 50px; }
        //     ul.SexyChoiceForm input:checked + label .SexyChoiceForm-button.purple { background: rebeccapurple; }
        //     ul.SexyChoiceForm input:checked + label .SexyChoiceForm-button.red { background: red; }
        //     ul.SexyChoiceForm input:checked + label .SexyChoiceForm-button.blue { background: lightblue; }
        //     ul.SexyChoiceForm input:checked + label .SexyChoiceForm-button.green { background: green; }
        // ');

        $needs = NeedsAndOffers::get();
        if ($needs->exists()) {
            $Needs = new Select2Field('Needs', _t("$name.Needs", "Compétence(s) et champ(s) d'expertise"), $needs->sort('Title')->map()->toArray());
            $Needs->setMultiple(true);
            $this->Fields()->push($Needs);
        }
    }

    public function doSave($data)
    {
        parent::doSave($data);
    }
}
