<?php

/**
 * IOPMember
 *
 * @author Johann
 * @extends Member
 */
class IOPMember extends DataExtension
{
    const GROUP_LEVEL_0 = 'level0';
    const GROUP_LEVEL_1 = 'level1';
    const GROUP_LEVEL_2 = 'level2';

    private static $db = array(
        // 'IOPCustom1' => 'Varchar',
        // 'IOPCustom2' => 'Varchar',
        // 'IOPCustom3' => 'Varchar',
        // 'IOPCustom4' => 'Varchar',
        // 'IOPCustom5' => 'Varchar',
        // 'IOPCustom6' => 'Varchar',
        // 'IOPCustom7' => 'Varchar',
        // 'IOPCustom8' => 'Varchar',
        // 'IOPCustom9' => 'Varchar',
        'IOPCollaborate' => 'Varchar(1)',
        'IOPAnticipate'  => 'Varchar(1)',
        'IOPNumeric'     => 'Varchar(1)',
        'IOPSocials'     => 'Varchar(1)',
        'IOPAvailable'   => 'Varchar(1)',
        'IOPMatchingCode' => 'Varchar(255)', //[LDC-2021.06.02] #3356 create matching function between members
        'IOPOtherInterest' => 'Varchar(255)', // [LDC-2021.06.21] #3443
    );

    // public static function FieldIOPCustomList()
    // {
    //     return array(
    //         ''  => "Je ne sais pas",
    //         '1' => "1",
    //         '2' => "2",
    //         '3' => "3",
    //     );
    // }

    // public static function FieldIOPCustomField($fieldname, $fieldtitle)
    // {
    //     return new DropdownField($fieldname, $fieldtitle, IOPMember::FieldIOPCustomList());
    // }

    public function updateCMSFields(FieldList $fields)
    {
        // $IOPTab = $fields->findOrMakeTab('Root.IOP');
        // foreach (range(1,9) as $i) {
        //     $IOPTab->push(IOPMember::FieldIOPCustomField('IOPCustom'. $i, 'IOPCustom'. $i));
        // }
        $fields->addFieldToTab('Root.Main', 
            new DropdownField('IOPCollaborate', _t('RegisterForm.IOPCollaborate'), IOPMember::FieldIOPCollaborateList()));
        $fields->addFieldToTab('Root.Main', 
            new DropdownField('IOPAnticipate', _t('RegisterForm.IOPAnticipate'), IOPMember::FieldIOPAnticipateList()));
        $fields->addFieldToTab('Root.Main', 
            new DropdownField('IOPNumeric', _t('RegisterForm.IOPNumeric'), IOPMember::FieldIOPNumericList()));
        $fields->addFieldToTab('Root.Main', 
            new DropdownField('IOPSocials', _t('RegisterForm.IOPSocials'), IOPMember::FieldIOPSocialsList()));
        $fields->addFieldToTab('Root.Main', 
            new DropdownField('IOPAvailable', _t('RegisterForm.IOPAvailable'), IOPMember::FieldIOPAvailableList()));
    }

    public static function FieldIOPCollaborateList()
    {
        return array(
            '0'  => "Choisir",
            '1' => "Faiblement motivé.e",
            '2' => "Motivé.e",
            '3' => "Très motivé.e",
        );
    }

    public static function FieldIOPAnticipateList()
    {
        return array(
            '0'  => "Choisir",
            '1' => "Je ne crois pas",
            '2' => "Plutôt convaincu.e",
            '3' => "Entièrement convaincu.e",
        );
    }

    public static function FieldIOPNumericList()
    {
        return array(
            '0'  => "Choisir",
            '1' => "Peu favorable",
            '2' => "Favorable",
            '3' => "Très favorable",
        );
    }

    public static function FieldIOPSocialsList()
    {
        return array(
            '0'  => "Choisir",
            '1' => "Faible priorité",
            '2' => "Priorité moyenne",
            '3' => "Forte priorité",
        );
    }

    public static function FieldIOPAvailableList()
    {
        return array(
            '0'  => "Choisir",
            '1' => "Peu disponible",
            '2' => "Disponible",
            '3' => "Très disponible",
        );
    }

    public function updateSummaryFields(&$fields)
    {
    }

    public function onBeforeWrite()
    {
        if ($this->owner->ID) {
            $Group = Group::get()->filter('Code', self::GROUP_LEVEL_1)->first();
            $Group->Members()->add($this->owner);

            if ($this->owner->hasCompleteProfile()) {
                $Group = Group::get()->filter('Code', self::GROUP_LEVEL_2)->first();
                $Group->Members()->add($this->owner);
                //[LDC-2021.06.01] #3356 generate code for matching function
                $this->GenerateMatchingCode();
            }
        }
    }

    /**
     * Add default records to database.
     *
     * This function is called whenever the database is built, after the
     * database tables have all been created.
     */
    public function requireDefaultRecords()
    {
        // Create default groups.
        foreach ([self::GROUP_LEVEL_0,self::GROUP_LEVEL_1,self::GROUP_LEVEL_2] as $i => $code) {
            $Group = Group::get()->filter('Code', $code)->first();
            if(!$Group) {
                $Group = new Group();
                $Group->Code = $code;
                $Group->Title = "Niveau $i";
                $Group->write();
            }
        }
    }

     /**
     * @since [LDC-2021.06.01] Generate and save a "matching code" from the registre form answers
     */
    public function GenerateMatchingCode()
    {
        $MatchingCode = $this->owner->IOPCollaborate . $this->owner->IOPAnticipate . $this->owner->IOPNumeric . $this->owner->IOPSocials . $this->owner->IOPAvailable;
        $this->owner->IOPMatchingCode = $MatchingCode;
        //SS_Log::Log(__FUNCTION__ . ' MATHING CODE ' . $MatchingCode, SS_Log::INFO);
    }
}
