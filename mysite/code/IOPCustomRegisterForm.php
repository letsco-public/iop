<?php

/**
 * IOPCustomRegisterForm
 *
 * @author Johann
 * @extends CustomRegisterForm
 */
class IOPCustomRegisterForm extends CustomRegisterForm
{
    // const FieldIOPCustom1 = 'IOPCustom1';
    // const FieldIOPCustom2 = 'IOPCustom2';
    // const FieldIOPCustom3 = 'IOPCustom3';
    // const FieldIOPCustom4 = 'IOPCustom4';
    // const FieldIOPCustom5 = 'IOPCustom5';
    // const FieldIOPCustom6 = 'IOPCustom6';
    // const FieldIOPCustom7 = 'IOPCustom7';
    // const FieldIOPCustom8 = 'IOPCustom8';
    // const FieldIOPCustom9 = 'IOPCustom9';
    const FieldIOPCollaborate            = 'IOPCollaborate';
    const FieldIOPAnticipate             = 'IOPAnticipate';
    const FieldIOPNumeric                = 'IOPNumeric';
    const FieldIOPSocials                = 'IOPSocials';
    const FieldIOPAvailable              = 'IOPAvailable';
    const FieldMessageTexte              = 'MessageTexte'; #3364 [LDC-2021.06.07]
    const FieldCheckBoxICantFindInterest = 'CheckBoxICantFindInterest'; #3443 [LDC-2021.06.21]
    const FieldIOPOtherInterest          = 'IOPOtherInterest'; #3443 [LDC-2021.06.21]

    protected function updateListFields($fields)
    {
        return [
            // 'FieldIOPCustom1' => self::FieldIOPCustom1,
            // 'FieldIOPCustom2' => self::FieldIOPCustom2,
            // 'FieldIOPCustom3' => self::FieldIOPCustom3,
            // 'FieldIOPCustom4' => self::FieldIOPCustom4,
            // 'FieldIOPCustom5' => self::FieldIOPCustom5,
            // 'FieldIOPCustom6' => self::FieldIOPCustom6,
            // 'FieldIOPCustom7' => self::FieldIOPCustom7,
            // 'FieldIOPCustom8' => self::FieldIOPCustom8,
            // 'FieldIOPCustom9' => self::FieldIOPCustom9,
            'FieldIOPCollaborate'               => self::FieldIOPCollaborate,
            'FieldIOPAnticipate'                => self::FieldIOPAnticipate,
            'FieldIOPNumeric'                   => self::FieldIOPNumeric,
            'FieldIOPSocials'                   => self::FieldIOPSocials,
            'FieldIOPAvailable'                 => self::FieldIOPAvailable,
            'FieldMessageTexte'                 => self::FieldMessageTexte, #3364 [LDC-2021.06.07]
            'FieldCheckBoxICantFindInterest'    => self::FieldCheckBoxICantFindInterest, #3443 [LDC-2021.06.21]
            'FieldIOPOtherInterest'             => self::FieldIOPOtherInterest, #3443 [LDC-2021.06.21]
        ];
    }

    protected function updateCreateFieldByName($fieldname, $fieldtitle)
    {
        $member = Member::currentUser();

        switch ($fieldname) {
            // case self::FieldIOPCustom1:
            //     return IOPMember::FieldIOPCustomField($fieldname, $fieldtitle);
            //     break;
            case self::FieldIOPCollaborate:
                $field = new DropdownField('IOPCollaborate', _t('RegisterForm.IOPCollaborate'), IOPMember::FieldIOPCollaborateList()); #3364 [LDC-2021.06.07] 
                $field->setTooltip( _t('RegisterForm.IOPCollaborate_Tooltip'));
                return $field;
            case self::FieldIOPAnticipate:
                $field = new DropdownField('IOPAnticipate', _t('RegisterForm.IOPAnticipate'), IOPMember::FieldIOPAnticipateList());
                $field->setTooltip( _t('RegisterForm.IOPAnticipate_Tooltip'));
                return $field;
            case self::FieldIOPNumeric:
                $field = new DropdownField('IOPNumeric', _t('RegisterForm.IOPNumeric'), IOPMember::FieldIOPNumericList());
                $field->setTooltip( _t('RegisterForm.IOPNumeric_Tooltip'));
                return $field;
            case self::FieldIOPSocials:
                $field = new DropdownField('IOPSocials', _t('RegisterForm.IOPSocials'), IOPMember::FieldIOPSocialsList());
                $field->setTooltip( _t('RegisterForm.IOPSocials_Tooltip'));
                return $field;
            case self::FieldIOPAvailable:
                $field = new DropdownField('IOPAvailable', _t('RegisterForm.IOPAvailable'), IOPMember::FieldIOPAvailableList());
                $field->setTooltip( _t('RegisterForm.IOPAvailable_Tooltip'));
                return $field;
            case self::FieldMessageTexte: #3364 [LDC-2021.06.07]
                return new LiteralField('Message : En répondant aux questions suivantes vous aurez accès à toutes les fonctionnalités...','<div class="FieldMessageTexte uk-margin-large" style="text-align: justify; font-size: 14px; font-style: italic;">
                    En répondant aux questions suivantes vous aurez accès à toutes les fonctionnalités d\'IOP!. Vous pourrez notamment solliciter les membres d\'IOP!, établir des collaborations, consulter des suggestions de profils pouvant vous intéresser ou correspondant à vos besoins.</div>');
            case self::FieldCheckBoxICantFindInterest: #3443 [LDC-2021.06.21]
                return new CheckboxField('CheckBoxICantFindInterest', _t('RegisterForm.CheckBoxICantFindInterest', "Je ne trouve pas la thématique me correspondant dans la liste"));
            case self::FieldIOPOtherInterest: #3443 [LDC-2021.06.21]
                return new TextField('IOPOtherInterest', _t('RegisterForm.IOPOtherInterest', "Saisissez ici votre thématique"));
            }
        return null;
    }

    // public function doSave($data)
    // {
    //     return parent::doSave($data);
    // }
}
