<?php

class RESTfulAPI_RCQueryHandler extends RESTfulAPI_DefaultQueryHandler
{

  /**
   * Create object of class $model
   * 
   * @param  string         $model
   * @param  SS_HTTPRequest $request
   * @return DataObject
   */
  public function createModel($model, SS_HTTPRequest $request)
  {
    $newModel = parent::createModel($model, $request);

    if ($newModel instanceof RESTfulAPI_Error) return $newModel;

    // $apiInstance = RestfulAPI::create();
    // if ($apiInstance->authenticator && $apiInstance->authority) {
    //   $member = $apiInstance->authenticator->getOwner($request);
    // }
    $member = Member::currentUser();

    // Set object to member subsite(s).
    SS_Log::Log("RESTfulAPI_RCQueryHandler.createModel($member->ID) >> object: ". get_class($newModel) .'('. $newModel->ID .')', SS_Log::INFO);
    if ($newModel->hasMethod('Subsite')) {
      SS_Log::Log("RESTfulAPI_RCQueryHandler.createModel($member->ID) >> object: ". get_class($newModel) .'('. $newModel->ID .') >> set model to main member\'s Subiste: '. $member->RegisterFromSubsiteID, SS_Log::INFO);
      $newModel->SubsiteID = $member->RegisterFromSubsiteID;
    } elseif ($newModel->hasMethod('Subsites')) {
      foreach ($member->Subsites() as $subsite) {
        SS_Log::Log("RESTfulAPI_RCQueryHandler.createModel($member->ID) >> object: ". get_class($newModel) .'('. $newModel->ID .') >> set model to each member Subiste: '. $subsite->ID, SS_Log::INFO);
        $newModel->Subsites()->add($subsite);
      }
    }

    if ($newModel->hasMethod('RegisterFromSubsite')) {
      $newModel->RegisterFromSubsiteID = $member->RegisterFromSubsiteID;
    }
    $newModel->write();

    return $newModel;
  }

}
