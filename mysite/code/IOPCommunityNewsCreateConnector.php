<?php

/**
 * IOPCommunityNewsCreateConnector
 *
 * @author Johann
 * @since #3107 Zapier-like workflow
 * @extends SilverStripeCommunityNewsCreateConnector
 */
class IOPCommunityNewsCreateConnector extends SilverStripeCommunityNewsCreateConnector
{
    public function Name()
    {
        return _t(__CLASS__ .'.Name', '[IOP] Créer une actualité');
    }

    public function _run($WorkflowAction, $Member, $dataobject)
    {
        // SS_Log::Log(__CLASS__.'.'.__FUNCTION__ .'('. get_class($dataobject) .'/'. $dataobject->ID .')', SS_Log::INFO);
    	if ($News = parent::_run($WorkflowAction, $Member, $dataobject)) {
	        $News->IOPType = 'News';
	        $News->write();
    	}
    }
}
