<?php

/**
 * IOPCommunityNewsPage
 *
 * @author Johann
 * @extends CommunityNewsPage
 */
class IOPCommunityNewsPage extends DataExtension
{
	const IOPType = 'IOPType';

    /**
     * @see FilterPageExtension.listFilters
     */
    public function updateListFilters(&$filters)
    {
        $filters = array_merge($filters, array(
            IOPCommunityNewsPage::IOPType,
        ));
    }
}

/**
 * IOPCommunityNewsPage_Controller
 *
 * @author Johann
 * @extends CommunityNewsPage_Controller
 */
class IOPCommunityNewsPage_Controller extends Extension
{
    public function init()
    {
        Requirements::css('mlbc/css/ribbon.css');
    }
    
    /**
     * @see FilterPageExtension_Controller.FilterForm
     */
    public function updateFilterForm($key, $form)
    {
        if (IOPCommunityNewsPage::IOPType == $key) {
            $form->createDropFilter($key, "Type", IOPCommunityNews::listIOPSimpleTypes());
            $form->setCallback($key, function($list, $values) {
                if (!empty($values)) {
                    $list = $list->filter('IOPType', $values);
                }
                return $list;
            });
        }
    }
}
