<?php

/**
 * IOPResourceForm
 *
 * @author Johann
 * @extends MlbcResourceForm
 */
class IOPResourceForm extends MlbcResourceForm
{
    protected function defineValidator(Controller $controller, FieldList $fields)
    {
        $validator = parent::defineValidator($controller, $fields);
        $validator->addRequiredFields(array('Interests'));
        return $validator;
    }
}
