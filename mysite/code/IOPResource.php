<?php

/**
 * IOPResource
 *
 * @author Johann
 * @extends MlbcResource
 */
class IOPResource extends DataExtension
{
    private static $db = array(
        'IOPAim' => 'Enum(",hypothese,dynamique,vision,prototype")',
        'IOPDuration' => 'Enum(",xs,s,m,l,xl")',
        'IOPCount' => 'Varchar',
    );

    public static function listIOPAims()
    {
        return [
            '' => "Choisir",
            'hypothese' => "Formuler des hypothèses sur le futur",
            'dynamique' => "Explorer les dynamiques de changement du « système » dont vous êtes acteur·rice (une entreprise, une institution, un secteur d’activité, un territoire…)",
            'vision'    => "Développer une vision – Challenger sa vision",
            'route'     => "Développer une feuille de route – Challenger sa feuille de route",
            'prototype' => "Prototyper",
        ];
    }

    public static function listIOPDuration()
    {
        return [
            '' => "Choisir",
            'xs' => "30 minutes à 1 heure",
            's'  => "1 à 3 heures",
            'm'  => "0,5 à 1 jour",
            'l'  => "1 à 2 jours",
            'xl' => "Plus de 2 jours",
        ];
    }

    public static function listIOPCount()
    {
        return [
            '' => "Choisir",
            '1' => "Seul.e",
            '3' => "Minimum 3",
            '5' => "Minimum 5-7",
        ];
    }

    public function updateCMSFields(FieldList $fields)
    {
        $tabAnticipate = $fields->findOrMakeTab('Root.Anticipate');
        $tabAnticipate->setTitle("Anticiper");
        $tabAnticipate->push(new DropdownField('IOPAim', "Objectif", self::listIOPAims()));
        $tabAnticipate->push(new DropdownField('IOPDuration', "Temps à consacrer", self::listIOPDuration()));
        $tabAnticipate->push(new MlbcCheckboxSetField('IOPCount', "Nombre de participants", self::listIOPCount()));
    }

    public function updateSummaryFields(&$fields)
    {
    }

    public function onBeforeWrite()
    {
        // https://gist.github.com/umidjons/11037635
        // https://stackoverflow.com/questions/467793/how-do-i-convert-a-pdf-document-to-a-preview-image-in-php
        // $im = new Imagick();
        // $im->setResolution(300, 300);     //set the resolution of the resulting jpg
        // $im->readImage('file.pdf[0]');    //[0] for the first page
        // $im->setImageFormat('jpg');
    }
}
