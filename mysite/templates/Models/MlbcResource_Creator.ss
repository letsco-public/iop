<style type="text/css">

.send-msg-bnt-container {
    display: flex;
}

.send-msg-bnt-container > a {
    margin: auto !important;
}

</style>


<div class="uk-margin-large-top uk-margin-large-bottom uk-grid" style="border-bottom: 1px solid #d8d8d8; border-top: 1px solid #d8d8d8; padding-top: 23px; padding-bottom: 23px; ">
    <div class="uk-width-medium-2-3">
        <div class="pMemberIcon">
            <% if $Creator.AvatarThumb %>
            $Creator.AvatarThumb
            <% else %>
            <img src="/community/images/default_vignette_member.png" alt="" />
            <% end_if %>
        </div>
        <div class="pThird">
                <div class="pMember secondary-color"><a href="$Creator.Link">$Creator.PublicName</a></div>
                <div class="pSociety"><% if $Creator.SocialReason %><a href="$Top.ListableObject.Creator.Society.Link">$Creator.SocialReason</a><% end_if %></div>
                <div class="pDate">$NotifiedAt.Format('d/m/Y - H:i')</div>
        </div>
    </div>
    <div class="send-msg-bnt-container uk-width-medium-1-3">
        <% if $CurrentMember %>
            $Creator.SendMessageButton
        <% end_if %>
    </div>
</div>
