<div class="Tags">
   <% if Offers.exists && CurrentMember.hasCompleteProfile %>
    <h3 class="title_tags">Mes champs de compétences et d'expertise :</h3>
    <div class="Offers">
        <% loop Offers %>
          <a class="Interest" href="$Top.Link?needs=$ID">$Title</a>
        <% end_loop %>
    </div>
    <% end_if %>
</div>